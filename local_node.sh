#!/bin/bash

KEYS[0]="valid0"
KEYS[1]="dev0"
KEYS[2]="dev1"
KEYS[3]="dev2"
KEYS[4]="dev3"

CHAINID="relictum_9000-1"
MONIKER="localtestnet"
KEYRING="test"
KEYALGO="eth_secp256k1"
LOGLEVEL="info"
HOMEDIR="$HOME/.relictumd"
TRACE=""

CONFIG=$HOMEDIR/config/config.toml
APP_TOML=$HOMEDIR/config/app.toml
GENESIS=$HOMEDIR/config/genesis.json
TMP_GENESIS=$HOMEDIR/config/tmp_genesis.json

make install

rm -rf "$HOMEDIR"

relictumd config keyring-backend $KEYRING --home "$HOMEDIR"
relictumd config chain-id $CHAINID --home "$HOMEDIR"

for KEY in "${KEYS[@]}"; do
	relictumd keys add "$KEY" --keyring-backend $KEYRING --algo $KEYALGO --home "$HOMEDIR"
done

relictumd init $MONIKER -o --chain-id $CHAINID --home "$HOMEDIR"

jq '.app_state["staking"]["params"]["bond_denom"]="aght"' "$GENESIS" >"$TMP_GENESIS" && mv "$TMP_GENESIS" "$GENESIS"
jq '.app_state["crisis"]["constant_fee"]["denom"]="aght"' "$GENESIS" >"$TMP_GENESIS" && mv "$TMP_GENESIS" "$GENESIS"
jq '.app_state["gov"]["deposit_params"]["min_deposit"][0]["denom"]="aght"' "$GENESIS" >"$TMP_GENESIS" && mv "$TMP_GENESIS" "$GENESIS"
jq '.app_state["gov"]["params"]["min_deposit"][0]["denom"]="aght"' "$GENESIS" >"$TMP_GENESIS" && mv "$TMP_GENESIS" "$GENESIS"
jq '.app_state["evm"]["params"]["evm_denom"]="aght"' "$GENESIS" >"$TMP_GENESIS" && mv "$TMP_GENESIS" "$GENESIS"
jq '.app_state["inflation"]["params"]["mint_denom"]="aght"' "$GENESIS" >"$TMP_GENESIS" && mv "$TMP_GENESIS" "$GENESIS"

for KEY in "${KEYS[@]}"; do
    relictumd add-genesis-account "$KEY" 1000000000000000000000000000aght --keyring-backend $KEYRING --home "$HOMEDIR"
done

relictumd gentx "${KEYS[0]}" 1000000000000000000000aght --keyring-backend $KEYRING --chain-id $CHAINID --home "$HOMEDIR"

relictumd collect-gentxs --home "$HOMEDIR"
relictumd validate-genesis --home "$HOMEDIR"

relictumd start --home "$HOMEDIR"
